package com.template.flows

import co.paralleluniverse.fibers.Suspendable
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.node.StatesToRecord
import net.corda.core.transactions.SignedTransaction

@StartableByRPC
class IOUFlowFullCycle(override var iouValue: Int,
                       override var otherParty: Party) : IOUFlow(iouValue, otherParty) {
    @Suspendable
    override fun call(): SignedTransaction {
        val finalTx = super.call()

        val regulator = serviceHub.identityService.partiesFromName("Regulator", true).single()
        subFlow(ReportToRegulatorFlow(regulator, finalTx))
        return finalTx
    }
}

@InitiatingFlow
class ReportToRegulatorFlow(private val regulator: Party, private val finalTx: SignedTransaction) : FlowLogic<Unit>() {
    @Suspendable
    override fun call() {
        val session = initiateFlow(regulator)
        subFlow(SendTransactionFlow(session, finalTx))
    }
}

@InitiatedBy(ReportToRegulatorFlow::class)
class ReceiveRegulatoryReportFlow(private val otherSideSession: FlowSession) : FlowLogic<Unit>() {
    @Suspendable
    override fun call() {
        subFlow(ReceiveTransactionFlow(otherSideSession, true, StatesToRecord.ALL_VISIBLE))
    }
}