package com.template.webserver

import net.corda.client.jackson.JacksonSupport
import org.springframework.boot.Banner
import org.springframework.boot.SpringApplication
import org.springframework.boot.WebApplicationType.SERVLET
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter

@SpringBootApplication
@Configuration
open class Starter {

    @Bean
    open fun converter(): MappingJackson2HttpMessageConverter {
        return MappingJackson2HttpMessageConverter(JacksonSupport.createNonRpcMapper())
    }
}

fun main(args: Array<String>) {
    val app = SpringApplication(Starter::class.java)
    app.setBannerMode(Banner.Mode.OFF)
    app.webApplicationType = SERVLET
    app.run(*args)
}
