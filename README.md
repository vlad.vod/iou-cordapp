# CorDapp IOUs

## Build

```
   $ ./gradlew :deployNodes
```

## Running the nodes

```
   $ cd build/nodes
   $ ./runnodes.sh
```

## Interacting with the nodes

### Webserver

`clients/src/main/kotlin/com/template/webserver/` defines a simple Spring webserver that connects to a node via RPC and 
allows you to interact with the node over HTTP.

The API endpoints are defined here:

     clients/src/main/kotlin/com/template/webserver/Controller.kt

#### Running the webserver

```
   $ ./gradlew :clients:runPartyAServer
   $ ./gradlew :clients:runPartyBServer
```

#### Interacting with the webserver

 - PartyAServer will be available at
    
```
   http://localhost:10050
```
    
    
 - PartyBServer will be available at
    
```
   http://localhost:10065
```
