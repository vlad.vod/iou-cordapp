package com.template.contracts

import com.template.IOUContract
import com.template.IOUContract.Commands.Create
import com.template.IOUState
import net.corda.core.identity.CordaX500Name
import net.corda.testing.core.TestIdentity
import net.corda.testing.node.MockServices
import net.corda.testing.node.ledger
import org.junit.Test

class IOUContractTests {

    private val lender = TestIdentity(CordaX500Name("Bank", "London", "GB"))
    private val borrower = TestIdentity(CordaX500Name("Shop", "Aberdeen", "GB"))
    private val ledgerServices = MockServices(listOf("com.template"), borrower, lender)

    private val iouState = IOUState(25, lender = lender.party, borrower = borrower.party)
    private val iouStateIssuedItself = IOUState(25, lender = borrower.party, borrower = borrower.party)
    private val iouStateWithNegativeValue = IOUState(-1, lender = lender.party, borrower = borrower.party)

    @Test
    fun `No inputs should be consumed when issuing an IOU`() {
        ledgerServices.ledger {

            transaction {
                input(IOUContract.ID, iouState)
                command(listOf(lender.publicKey, borrower.publicKey), Create())
                `fails with`("No inputs should be consumed when issuing an IOU.")
            }
        }
    }

    @Test
    fun `simple IOU issuance`() {
        ledgerServices.ledger {

            transaction {
                attachment(IOUContract.ID)
                command(listOf(lender.publicKey, borrower.publicKey), Create())
                output(IOUContract.ID, iouState)
                verifies()
            }
        }
    }

    @Test
    fun `simple IOU issuance to itself`() {
        ledgerServices.ledger {

            transaction {
                attachment(IOUContract.ID)
                command(listOf(lender.publicKey, borrower.publicKey), Create())
                output(IOUContract.ID, iouStateIssuedItself)
                `fails with`("The lender and the borrower cannot be the same entity.")
            }
        }
    }

    @Test
    fun `There must be two signers only`() {
        ledgerServices.ledger {

            transaction {
                attachment(IOUContract.ID)
                command(listOf(lender.publicKey, lender.publicKey), Create())
                output(IOUContract.ID, iouState)
                `fails with`("There must be two signers.")
            }
        }
    }

    @Test
    fun `The IOU's value must be non-negative`() {
        ledgerServices.ledger {

            transaction {
                attachment(IOUContract.ID)
                command(listOf(lender.publicKey, borrower.publicKey), Create())
                output(IOUContract.ID, iouStateWithNegativeValue)
                `fails with`("The IOU's value must be non-negative.")
            }
        }
    }
}
